from django.urls import path
from .views import HomeView,AboutView,AllProductsView,ProductDetailView

app_name = "ecomapp"

urlpatterns = [
    path("", HomeView.as_view(), name="home"),
    path("about/",AboutView.as_view(), name="about"),
    path("all-products/",AllProductsView.as_view(), name="allproducts"),
    path("products/<slug:slug>/",ProductDetailView.as_view(), name="productdetail"),
]